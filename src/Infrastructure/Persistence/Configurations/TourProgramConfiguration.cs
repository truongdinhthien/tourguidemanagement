using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Domain.Entities;

namespace Infrastructure.Persistence.Configurations
{
    public class TourProgramConfiguration : IEntityTypeConfiguration<TourProgram>
    {
        public void Configure(EntityTypeBuilder<TourProgram> builder)
        {
            builder.Property(tp => tp.Name).IsRequired();
            builder.Property(tp => tp.Code).IsRequired();
            builder.HasIndex(tp => tp.Code).IsUnique();
            builder.Property(tp => tp.Duration).IsRequired();
            builder.Property(tp => tp.Description).IsRequired();
        }
    }
}
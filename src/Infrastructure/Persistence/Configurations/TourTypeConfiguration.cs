using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configurations
{
    public class TourTypeConfiguration : IEntityTypeConfiguration<TourType>
    {
        public void Configure(EntityTypeBuilder<TourType> builder)
        {
            builder.Property(tt => tt.Name).IsRequired().HasMaxLength(200);
        }
    }
}
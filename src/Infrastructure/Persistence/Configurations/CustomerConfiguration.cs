using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Domain.Entities;

namespace Infrastructure.Persistence.Configurations
{
    public class CustomerConfiguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.OwnsOne(c => c.Address);

            builder.Property(c => c.IdentityCard).IsRequired();
            builder.HasIndex(c => c.IdentityCard).IsUnique();

            builder.Property(c => c.Email).IsRequired();
            builder.HasIndex(c => c.Email).IsUnique();

            builder.Property(c => c.Phone).IsRequired();
            builder.HasIndex(c => c.Phone).IsUnique();
        }
    }
}
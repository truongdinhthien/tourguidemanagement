using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Domain.Entities;

namespace Infrastructure.Persistence.Configurations
{
    public class TourPlacesConfiguration : IEntityTypeConfiguration<TourPlace>
    {
        public void Configure(EntityTypeBuilder<TourPlace> builder)
        {
            builder.HasKey(tp => new { tp.PlaceId, tp.TourProgramId });
            builder.Property(tp => tp.OrdinalNumber).IsRequired();
        }
    }
}
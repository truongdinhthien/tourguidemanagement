using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Domain.Entities;

namespace Infrastructure.Persistence.Configurations
{
    public class TourConfiguration : IEntityTypeConfiguration<Tour>
    {
        public void Configure(EntityTypeBuilder<Tour> builder)
        {
            builder.Property(t => t.Price).IsRequired();
            builder.Property(t => t.Code).IsRequired();
            builder.HasIndex(t => t.Code).IsUnique();
            builder.Property(t => t.DateStart).IsRequired();
            builder.Property(t => t.Quantity).IsRequired();
            builder.Property(t => t.TourProgramId).IsRequired();
        }
    }
}
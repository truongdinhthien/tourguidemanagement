

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Domain.Entities;

namespace Infrastructure.Persistence.Configurations
{
    public class TourServiceConfiguration : IEntityTypeConfiguration<TourService>
    {
        public void Configure(EntityTypeBuilder<TourService> builder)
        {
            builder.HasKey(ts => new { ts.ServiceId, ts.TourProgramId });
        }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.Enums;
using Domain.ValueObject;

namespace Infrastructure.Persistence
{
    public class ApplicationDbContextSeed
    {
        public static async Task Initialize(ApplicationDbContext context)
        {
            if (!context.TourTypes.Any())
            {
                await context.TourTypes.AddRangeAsync(
                    new List<TourType>
                    {
                        new TourType { Name="Du lịch di động" },
                        new TourType { Name="Du lịch kết hợp nghề nghiệp" },
                        new TourType { Name="Du lịch xã hội đoàn trường" },
                        new TourType { Name="Du lịch ngoại khóa nghiên cứu" },
                    }
                );
                await context.SaveChangesAsync();
            }
            if (!context.Places.Any())
            {
                await context.Places.AddRangeAsync(
                    new List<Place>
                    {
                        new Place {Name="Phan Thiết"},
                        new Place {Name="Nha Trang"},
                        new Place {Name="Đà Lạt"},
                        new Place {Name="Vũng Tàu"},
                        new Place {Name="Hồ Chí Minh"},
                    }
                );
                await context.SaveChangesAsync();
            }
            if (!context.Hotels.Any())
            {
                await context.Hotels.AddRangeAsync(
                    new List<Hotel>
                    {
                        new Hotel {
                            Name = "Khách sạn mây mưa",
                            Address = new Address("Đường 1", "Phường 1", "Quận 1", "Hồ Chí Minh", "Việt Nam"),
                        },
                        new Hotel {
                            Name = "Khách sạn vui vẻ",
                            Address = new Address("Đường 2", "Phường 2", "Quận 2", "Hồ Chí Minh", "Việt Nam"),
                        },
                        new Hotel {
                            Name = "Khách sạn thiên đàng",
                            Address = new Address("Đường 3", "Phường 3", "Quận 3", "Hồ Chí Minh ", "Việt Nam"),
                        },
                        new Hotel {
                            Name = "Khách sạn thần tiên",
                            Address = new Address("Đường 4", "Phường 4", "Quận 4", "Hồ Chí Minh", "Việt Nam"),
                        },
                    }
                );
                await context.SaveChangesAsync();
            }
            if (!context.Services.Any()) {
                await context.Services.AddRangeAsync(
                    new List<Service>
                    {
                        new Service {
                            Name = "Ăn sáng",
                            Price = 50000
                        },
                        new Service {
                            Name = "Ăn trưa",
                            Price = 60000
                        },
                        new Service {
                            Name = "Ăn chiều",
                            Price = 70000
                        },
                        new Service {
                            Name = "Ăn tối",
                            Price = 50000
                        },
                        new Service {
                            Name = "Phí cầu đường",
                            Price = 60000
                        },
                        new Service {
                            Name = "Phí gì đó",
                            Price = 70000
                        },
                    }
                );
                await context.SaveChangesAsync();
            }
            if(!context.Customers.Any())
            {
                 await context.Customers.AddRangeAsync(
                    new List<Customer>
                    {
                        new Customer {
                            Name="Khách hàng 1",
                            IdentityCard="111111111",
                            Phone="0911111111",
                            Email="customer1@gmail.com",
                            Gender = Gender.Male,
                            Address = new Address("Đường 1", "Phường 1", "Quận 1", "Hồ Chí Minh", "Việt Nam"),
                        },
                        new Customer {
                            Name="Khách hàng 2",
                            IdentityCard="222222222",
                            Phone="09222222222",
                            Email="customer2@gmail.com",
                            Gender = Gender.Female,
                            Address = new Address("Đường 2", "Phường 2", "Quận 2", "Hồ Chí Minh", "Việt Nam"),
                        },
                        new Customer {
                            Name="Khách hàng 3",
                            IdentityCard="333333333",
                            Phone="0933333333",
                            Email="customer3@gmail.com",
                            Gender = Gender.Male,
                            Address = new Address("Đường 3", "Phường 3", "Quận 3", "Hồ Chí Minh", "Việt Nam"),
                        },
                    }
                );
                await context.SaveChangesAsync();
            }
            if(!context.Staffs.Any())
            {
                 await context.Staffs.AddRangeAsync(
                    new List<Staff>
                    {
                        new Staff {
                            Name="Nhân viên 1",
                            IdentityCard="111111111",
                            Phone="0911111111",
                            Email="staff1@gmail.com",
                            Gender = Gender.Male,
                            Address = new Address("Đường 1", "Phường 1", "Quận 1", "Hồ Chí Minh", "Việt Nam"),
                            Role = Role.BusDriver
                        },
                        new Staff {
                            Name="Nhân viên 2",
                            IdentityCard="222222222",
                            Phone="09222222222",
                            Email="staff2@gmail.com",
                            Gender = Gender.Female,
                            Address = new Address("Đường 2", "Phường 2", "Quận 2", "Hồ Chí Minh", "Việt Nam"),
                            Role = Role.TourGuide,
                        },
                        new Staff {
                            Name="Nhân viên 3",
                            IdentityCard="333333333",
                            Phone="0933333333",
                            Email="staff3@gmail.com",
                            Gender = Gender.Male,
                            Address = new Address("Đường 3", "Phường 3", "Quận 3", "Hồ Chí Minh", "Việt Nam"),
                            Role = Role.Leader,
                        },
                        new Staff {
                            Name="Nhân viên 4",
                            IdentityCard="444444444",
                            Phone="09444444444",
                            Email="staff4@gmail.com",
                            Gender = Gender.Male,
                            Address = new Address("Đường 4", "Phường 4", "Quận 4", "Hồ Chí Minh", "Việt Nam"),
                            Role = Role.TourGuide,
                        },
                    }
                );
                await context.SaveChangesAsync();
            }
            if (!context.TourPrograms.Any())
            {
                await context.TourPrograms.AddRangeAsync(
                    new List<TourProgram>
                    {
                        new TourProgram {
                            Name="Nha trang vui vẻ ",
                            Code="NHATRANGVUI",
                            Description="Long Description Here",
                            Duration=3,
                            TourTypeId=3,
                            HotelId=1,
                            PriceHotel=100000,
                            TourServices = new List<TourService> {
                                new TourService {
                                    ServiceId = 1,
                                },
                                new TourService {
                                    ServiceId = 3,
                                },
                                new TourService {
                                    ServiceId = 2,
                                },
                            }
                        },
                        new TourProgram {
                            Name="Đà Lạt Siêu Hấp Dẫn",
                            Code="DALAT",
                            Description="Long Description Here",
                            Duration=3,
                            TourTypeId=2,
                            HotelId=2,
                            PriceHotel=200000,
                            TourServices = new List<TourService> {
                                new TourService {
                                    ServiceId = 1,
                                },
                                new TourService {
                                    ServiceId = 3,
                                },
                                new TourService {
                                    ServiceId = 2,
                                },
                                new TourService {
                                    ServiceId = 5,
                                },
                            }
                        },
                    }
                );
                await context.SaveChangesAsync();
            }
            if (!context.Tours.Any())
            {
                await context.Tours.AddRangeAsync(
                    new List<Tour>
                    {
                        new Tour {
                            Code="NHATRANGVUI-20062020-1",
                            Price=2E6M,
                            TourProgramId=1,
                            DateStart=new DateTime(2020, 6, 20),
                            Quantity=40,
                            Assignments= new List<Assignment> {
                                new Assignment {
                                    StaffId = 1
                                },
                                new Assignment {
                                    StaffId = 2
                                },
                                new Assignment {
                                    StaffId = 3
                                },
                            }
                        },
                        new Tour {
                            Code="NHATRANGVUI-30062020-2",
                            Price=2E6M,
                            TourProgramId=1,
                            DateStart=new DateTime(2020, 6, 30),
                            Quantity=45,
                            Assignments= new List<Assignment> {
                                new Assignment {
                                    StaffId = 1
                                },
                                new Assignment {
                                    StaffId = 3
                                },
                                new Assignment {
                                    StaffId = 4
                                },
                            }
                        },
                        new Tour {
                            Code="NHATRANGVUI-01072020-3",
                            Price=1E8M,
                            TourProgramId=1,
                            DateStart=new DateTime(2020, 7, 1),
                            Quantity=50,
                            Assignments= new List<Assignment> {
                                new Assignment {
                                    StaffId = 1
                                },
                                new Assignment {
                                    StaffId = 3
                                },
                                new Assignment {
                                    StaffId = 2
                                },
                                new Assignment {
                                    StaffId = 4
                                },
                            }
                        },
                        new Tour {
                            Code="DALAT-21062020-1",
                            Price=2E6M,
                            TourProgramId=2,
                            DateStart=new DateTime(2020, 6, 21),
                            Quantity=40,
                            Assignments= new List<Assignment> {
                                new Assignment {
                                    StaffId = 1
                                },
                                new Assignment {
                                    StaffId = 3
                                },
                                new Assignment {
                                    StaffId = 2
                                },
                                new Assignment {
                                    StaffId = 4
                                },
                            }
                        },
                        new Tour {
                            Code="DALAT-02072020-2",
                            Price=2E6M,
                            TourProgramId=2,
                            DateStart=new DateTime(2020, 7, 2),
                            Quantity=45,
                            Assignments= new List<Assignment> {
                                new Assignment {
                                    StaffId = 1
                                },
                                new Assignment {
                                    StaffId = 3
                                },
                                new Assignment {
                                    StaffId = 2
                                },
                                new Assignment {
                                    StaffId = 4
                                },
                            }
                        },
                    }
                );
                await context.SaveChangesAsync();
            }
            if (!context.TourPlaces.Any())
            {
                await context.TourPlaces.AddRangeAsync(
                    new List<TourPlace>
                    {
                        new TourPlace {
                            TourProgramId=1,
                            PlaceId=5,
                            OrdinalNumber=1,
                        },
                        new TourPlace {
                            TourProgramId=1,
                            PlaceId=1,
                            OrdinalNumber=2,
                        },
                        new TourPlace {
                            TourProgramId=1,
                            PlaceId=2,
                            OrdinalNumber=3,
                        },
                        new TourPlace {
                            TourProgramId=2,
                            PlaceId=5,
                            OrdinalNumber=1,
                        },
                        new TourPlace {
                            TourProgramId=2,
                            PlaceId=4,
                            OrdinalNumber=2,
                        },
                    }
                );
                await context.SaveChangesAsync();
            }
            if(!context.Contracts.Any())
            {
                await context.Contracts.AddRangeAsync(
                    new List<Contract> {
                        new Contract {
                            CustomerId=1,
                            TourId=2,
                            Amount=5,
                        },
                        new Contract {
                            CustomerId=2,
                            TourId=1,
                            Amount=5,
                        },
                        new Contract {
                            CustomerId=3,
                            TourId=1,
                            Amount=6,
                        },
                        new Contract {
                            CustomerId=2,
                            TourId=4,
                            Amount=3,
                        },
                        new Contract {
                            CustomerId=3,
                            TourId=1,
                            Amount=3,
                        },
                        new Contract {
                            CustomerId=2,
                            TourId=5,
                            Amount=2,
                        }
                    }
                );

                await context.SaveChangesAsync();
            }
        }
    }
}
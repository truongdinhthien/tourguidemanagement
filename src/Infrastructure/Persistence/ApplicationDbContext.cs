using Microsoft.EntityFrameworkCore;
using Domain.Entities;
using System.Reflection;

namespace Infrastructure.Persistence
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) {}
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }
        public DbSet<TourType> TourTypes { get; set; }
        public DbSet<TourPlace> TourPlaces { get; set; }
        public DbSet<Tour> Tours { get; set; }
        public DbSet<Place> Places { get; set; }
        public DbSet<TourProgram> TourPrograms { get; set; }
        public DbSet<TourService> TourServices {get;set;}
        public DbSet<Service> Services {get;set;}
        public DbSet<Hotel> Hotels {get;set;}
        public DbSet<Contract> Contracts {get;set;}
        public DbSet<Assignment> Assignments {get;set;}
        public DbSet<Customer> Customers {get;set;}
        public DbSet<Staff> Staffs {get;set;}
    }
}
using Application.Common.Base;
using Application.Common.Interfaces;
using AutoMapper;
using Domain.Entities;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.TourTypes.Queries
{
    public class GetAllTourTypesQuery : IRequest<IEnumerable<TourTypeDTO>>
    {
    }

    public class GetAllTourTypesQueryHandler : BaseHandler, IRequestHandler<GetAllTourTypesQuery, IEnumerable<TourTypeDTO>>
    {
        public GetAllTourTypesQueryHandler(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper) {}
        public async Task<IEnumerable<TourTypeDTO>> Handle(GetAllTourTypesQuery request, CancellationToken cancellationToken)
        {
            var tourTypes = await _unitOfWork.Repository<TourType>().GetAllAsync();
            var tourTypesDto = _mapper.Map<IEnumerable<TourTypeDTO>>(tourTypes);
            return tourTypesDto;
        }
    }
}
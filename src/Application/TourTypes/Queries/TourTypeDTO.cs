namespace Application.TourTypes.Queries
{
    public class TourTypeDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
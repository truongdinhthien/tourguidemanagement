 
using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Base;
using Application.Common.Interfaces;
using AutoMapper;
using Application.Common.Exceptions;
using Domain.Entities;
using MediatR;

namespace Application.TourTypes.Commands
{
    public class UpdateTourTypeCommand : IRequest<object>
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }

    public class UpdateTourTypeCommandHandler : BaseHandler, IRequestHandler<UpdateTourTypeCommand, object>
    {
        public UpdateTourTypeCommandHandler(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper) { }
        public async Task<object> Handle(UpdateTourTypeCommand request, CancellationToken cancellationToken)
        {
            var tourType = await _unitOfWork.Repository<TourType>().GetByIdAsync(request.Id);
            if (tourType == null)
            {
                throw new NotFoundException("No Tourtype found", $"Please check your params id: {request.Id}");
            }
            tourType.Name = request.Name;
            _unitOfWork.Repository<TourType>().Update(tourType);
            await _unitOfWork.SaveChangesAsync(cancellationToken);
            return tourType;
        }
    }
}
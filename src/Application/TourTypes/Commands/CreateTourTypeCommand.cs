using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Base;
using Application.Common.Interfaces;
using AutoMapper;
using Domain.Entities;
using MediatR;

namespace Application.TourTypes.Commands
{
    public class CreateTourTypeCommand : IRequest<int>
    {
        [Required]
        public string Name { get; set; }
    }

    public class CreateTourTypeCommandHandler : BaseHandler, IRequestHandler<CreateTourTypeCommand, int>
    {
        public CreateTourTypeCommandHandler(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper) { }
        public async Task<int> Handle(CreateTourTypeCommand request, CancellationToken cancellationToken)
        {
            var tourTypes = new TourType
            {
                Name = request.Name,
            };
            await _unitOfWork.Repository<TourType>().AddAsync(tourTypes);
            await _unitOfWork.SaveChangesAsync(cancellationToken);
            return tourTypes.Id;
        }
    }
}
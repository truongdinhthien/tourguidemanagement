using System;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Base;
using Application.Common.Interfaces;
using AutoMapper;
using Application.Common.Exceptions;
using Domain.Entities;
using MediatR;

namespace Application.TourTypes.Commands
{
    public class DeleteTourTypeCommand : IRequest<bool>
    {
        [Required]
        public int Id { get; set; }
    }

    public class DeleteTourTypeCommandHandler : BaseHandler, IRequestHandler<DeleteTourTypeCommand, bool>
    {
        public DeleteTourTypeCommandHandler(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper) { }

        public async Task<bool> Handle(DeleteTourTypeCommand request, CancellationToken cancellationToken)
        {
            var tourType = await _unitOfWork.Repository<TourType>().GetByIdAsync(request.Id);
            if (tourType == null)
            {
                throw new NotFoundException("No Tourtype found", $"Please check your params id: {request.Id}");
            }
            _unitOfWork.Repository<TourType>().Remove(tourType);
            await _unitOfWork.SaveChangesAsync(cancellationToken);
            return true;
        }
    }
}

using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Base;
using Application.Common.Interfaces;
using Application.Customers.Queries;
using Application.TourAggregate.Queries.Specifications;
using AutoMapper;
using Domain.Entities;
using MediatR;

namespace Application.TourAggregate.Queries
{
    public class GetCustomerFromTourQuery : IRequest<IEnumerable<CustomerDTO>>
    {
        public int? TourId { get; set; }
    }

    public class GetCustomerFromTourQueryHandler : BaseHandler, IRequestHandler<GetCustomerFromTourQuery, IEnumerable<CustomerDTO>>
    {
        public GetCustomerFromTourQueryHandler(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper) { }
        public async Task<IEnumerable<CustomerDTO>> Handle(GetCustomerFromTourQuery request, CancellationToken cancellationToken)
        {
            var customerFromTour = await _unitOfWork.Repository<Customer>().GetAllAsync(new CustomerFromTourSpec(request.TourId));
            var customerDTOs = _mapper.Map<IEnumerable<Customer>, IEnumerable<CustomerDTO>>(customerFromTour);
            return customerDTOs;
        }
    }
}
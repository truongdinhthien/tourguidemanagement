using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Base;
using Application.Common.Interfaces;
using Application.TourAggregate.Queries.DTOs;
using Application.TourAggregate.Queries.Specifications;
using AutoMapper;
using Domain.Entities;
using MediatR;

namespace Application.TourAggregate.Queries
{
    public class GetTourProgramQuery : IRequest<IEnumerable<TourProgramDTO>>
    {
        
    }

    public class GetTourProgramQueryHandler : BaseHandler, IRequestHandler<GetTourProgramQuery, IEnumerable<TourProgramDTO>>
    {
        public GetTourProgramQueryHandler (IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper) {}
        public async Task<IEnumerable<TourProgramDTO>> Handle(GetTourProgramQuery request, CancellationToken cancellationToken)
        {
            var tourPrograms = await _unitOfWork.Repository<TourProgram>().GetAllAsync(new TourProgramIncludeTourTypeSpec());
            var tourProgramsDTO = _mapper.Map<IEnumerable<TourProgram>,IEnumerable<TourProgramDTO>>(tourPrograms);
            return tourProgramsDTO;
        }
    }
}
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Base;
using Application.Common.Interfaces;
using Application.TourAggregate.Queries.DTOs;
using Application.TourAggregate.Queries.Specifications;
using AutoMapper;
using Application.Common.Exceptions;
using Domain.Entities;
using MediatR;
using Application.TourAggregate.Queries.ViewModels;

namespace Application.TourAggregate.Queries
{
    public class GetTourProgramDetailByIdQuery : IRequest<TourProgramDetailViewModel>
    {
        public int Id { get; set; }
    }

    public class GetTourProgramDetailByIdQueryHandler : BaseHandler, IRequestHandler<GetTourProgramDetailByIdQuery, TourProgramDetailViewModel>
    {
        public GetTourProgramDetailByIdQueryHandler(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper) { }
        public async Task<TourProgramDetailViewModel> Handle(GetTourProgramDetailByIdQuery request, CancellationToken cancellationToken)
        {
            var tourProgramDetail = await _unitOfWork.Repository<TourProgram>().FirstOrDefaultAsync(new TourDetailByIdSpec(request.Id));
            if(tourProgramDetail == null)
            {
                throw new NotFoundException("No TourProgram Found", $"Please check your params id: {request.Id}");
            }
            var tourProgramDetailViewModel = _mapper.Map<TourProgram,TourProgramDetailViewModel>(tourProgramDetail);
            return tourProgramDetailViewModel;
        }
    }
}
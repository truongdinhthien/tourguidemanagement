using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Base;
using Application.Common.Interfaces;
using Application.TourAggregate.Queries.Specifications;
using Application.TourAggregate.Queries.ViewModels;
using AutoMapper;
using Domain.Entities;
using MediatR;

namespace Application.TourAggregate.Queries
{
     public class GetTourListQuery : IRequest<IEnumerable<TourListViewModel>>
    {
        
    }

    public class GetTourListQueryHandler : BaseHandler, IRequestHandler<GetTourListQuery, IEnumerable<TourListViewModel>>
    {
        public GetTourListQueryHandler (IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper) {}
        public async Task<IEnumerable<TourListViewModel>> Handle(GetTourListQuery request, CancellationToken cancellationToken)
        {
            var tours = await _unitOfWork.Repository<Tour>().GetAllAsync(new TourListSpec());
            var tourListVM = _mapper.Map<IEnumerable<Tour>,IEnumerable<TourListViewModel>>(tours);
            return tourListVM;
        }
    }
}
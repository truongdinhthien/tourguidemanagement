using System;

namespace Application.TourAggregate.Queries.DTOs
{
    public class TourDTO
    {
        public string Code { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public DateTime DateStart { get; set; }
    }
}
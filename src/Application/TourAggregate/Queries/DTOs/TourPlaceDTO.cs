namespace Application.TourAggregate.Queries.DTOs
{
    public class TourPlaceDTO
    {
        public int PlaceId { get; set; }
        public string PlaceName { get; set; }
        public int OrdinalNumber { get; set; }
    }
}
using Application.TourTypes.Queries;

namespace Application.TourAggregate.Queries.DTOs
{
    public class TourProgramDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int Duration { get; set; }
        public TourTypeDTO TourType { get; set; }
    }
}
using Application.Common.Base;
using Domain.Entities;

namespace Application.TourAggregate.Queries.Specifications
{
    public class TourDetailByIdSpec : Specification<TourProgram>
    {
        public TourDetailByIdSpec (int id) : base(t => t.Id == id)
        {
            AddInclude(t => t.Tours);
            AddInclude("TourPlaces.Place");
            AddInclude(t => t.TourType);
        }
    }
}
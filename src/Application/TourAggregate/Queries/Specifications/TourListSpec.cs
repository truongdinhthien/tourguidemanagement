using Application.Common.Base;
using Domain.Entities;

namespace Application.TourAggregate.Queries.Specifications
{
    public class TourListSpec : Specification<Tour>
    {
        public TourListSpec()
        {
            AddInclude(t => t.TourProgram);
        }
    }
}
using System.Linq;
using Application.Common.Base;
using Domain.Entities;

namespace Application.TourAggregate.Queries.Specifications
{
    public class CustomerFromTourSpec : Specification<Customer>
    {
        public CustomerFromTourSpec (int? TourId) : base(t => t.Contracts.Any(c => (!TourId.HasValue || c.TourId == TourId)))
        {

        }
    }
}
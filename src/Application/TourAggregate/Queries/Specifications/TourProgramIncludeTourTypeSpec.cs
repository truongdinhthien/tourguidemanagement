using Application.Common.Base;
using Domain.Entities;

namespace Application.TourAggregate.Queries.Specifications
{
    public class TourProgramIncludeTourTypeSpec : Specification<TourProgram>
    {
        public TourProgramIncludeTourTypeSpec () 
        {
            AddInclude(t => t.TourType);
        }

        public TourProgramIncludeTourTypeSpec (int id) : base (t => t.Id == id)
        {
            AddInclude(t => t.TourType);
        }
    }
}
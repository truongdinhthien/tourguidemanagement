using System;
using Application.TourAggregate.Queries.DTOs;

namespace Application.TourAggregate.Queries.ViewModels
{
    public class TourListViewModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public DateTime DateStart { get; set; }
        public TourProgramDTO TourProgram { get; set; }
    }
}
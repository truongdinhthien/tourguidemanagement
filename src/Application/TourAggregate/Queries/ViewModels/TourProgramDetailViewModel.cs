using System.Collections.Generic;
using Application.TourAggregate.Queries.DTOs;
using Application.TourTypes.Queries;
namespace Application.TourAggregate.Queries.ViewModels
{
    public class TourProgramDetailViewModel
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int Duration { get; set; }
        public TourTypeDTO TourType { get; set; }
        public ICollection<TourPlaceDTO> TourPlaces { get; set; }
        public ICollection<TourDTO> Tours { get; set; }
    }
}
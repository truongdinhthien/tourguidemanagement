using System;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Base;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using AutoMapper;
using Domain.Entities;
using MediatR;

namespace Application.TourAggregate.Commands.Create
{
    public class CreateTourPlaceCommand : IRequest<int>
    {
        [Required]
        public int? TourProgramId { get; set; }
        [Required]
        public int? PlaceId { get; set; }
        [Required]
        public int? OrdinalNumber { get; set; }
    }

    public class CreateTourPlaceCommandHandler : BaseHandler, IRequestHandler<CreateTourPlaceCommand, int>
    {
        public CreateTourPlaceCommandHandler(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {

        }

        public async Task<int> Handle(CreateTourPlaceCommand request, CancellationToken cancellationToken)
        {
            var tourPlace = _mapper.Map<CreateTourPlaceCommand, TourPlace>(request);
            await _unitOfWork.Repository<TourPlace>().AddAsync(tourPlace);
            var success = await _unitOfWork.SaveChangesAsync(cancellationToken) > 0;
            return tourPlace.PlaceId;
        }
    }
}
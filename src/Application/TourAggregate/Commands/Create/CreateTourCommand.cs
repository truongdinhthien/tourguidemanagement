using System;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Base;
using Application.Common.Interfaces;
using Application.Common.Exceptions;
using AutoMapper;
using Domain.Entities;
using MediatR;
using System.Linq;

namespace Application.TourAggregate.Commands.Create
{
    public class CreateTourCommand : IRequest<int>
    {
        [Required]
        public decimal? Price { get; set; }
        [Required]
        public int? Quantity { get; set; }
        [Required]
        public DateTime? DateStart { get; set; }
        [Required]
        public int? TourProgramId { get; set; }
    }

    public class CreateTourCommandHandler : BaseHandler, IRequestHandler<CreateTourCommand, int>
    {
        public CreateTourCommandHandler(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {

        }
        public async Task<int> Handle(CreateTourCommand request, CancellationToken cancellationToken)
        {
            var tourProgram = await _unitOfWork.Repository<TourProgram>().GetByIdAsync(request.TourProgramId.Value);
            if (tourProgram == null)
            {
                throw new NotFoundException("Data not found", $"Cannot found tourProgram {request.TourProgramId.Value}");
            }

            var tours = await _unitOfWork.Repository<Tour>().FindAsync(t => t.TourProgramId == tourProgram.Id);
            var tour = _mapper.Map<CreateTourCommand, Tour>(request);
            string FormatDateStart = tour.DateStart.ToShortDateString().Trim().Replace("/", "");
            tour.Code = tourProgram.Code + "-" + FormatDateStart + "-" + (tours.Count() + 1).ToString();
            await _unitOfWork.Repository<Tour>().AddAsync(tour);
            await _unitOfWork.SaveChangesAsync(cancellationToken);
            return tour.Id;
        }
    }
}
using Application.Common.Base;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using AutoMapper;
using Domain.Entities;
using MediatR;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using System;
using System.Linq;

namespace Application.TourAggregate.Commands.Create
{
    public class CreateTourProgramCommand : IRequest<int>
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Code { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public int? Duration { get; set; }
        [Required]
        public int? TourTypeId { get; set; }
    }

    public class CreateTourProgramCommandHandler : BaseHandler, IRequestHandler<CreateTourProgramCommand, int>
    {
        public CreateTourProgramCommandHandler (IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
            
        }

        public async Task<int> Handle(CreateTourProgramCommand request, CancellationToken cancellationToken)
        {
            var codeTourProgram = await _unitOfWork.Repository<TourProgram>().FindAsync(t=> t.Code == request.Code);
            if (codeTourProgram.Any())
            {
                throw new DuplicateException("Đuplicate data", $"{request.Code} is exist. Try another code");
            }

            var tourProgram = _mapper.Map<CreateTourProgramCommand, TourProgram>(request);
            await _unitOfWork.Repository<TourProgram>().AddAsync(tourProgram);
            await _unitOfWork.SaveChangesAsync(cancellationToken);
            return tourProgram.Id;
        }
    }
}
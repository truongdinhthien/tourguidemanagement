using Application.Common.Base;
using Application.Common.Interfaces;
using AutoMapper;
using Domain.Entities;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Customers.Queries
{
    public class GetAllCustomersQuery : IRequest<IEnumerable<CustomerDTO>>
    {
    }

    public class GetAllCustomersQueryHandler : BaseHandler, IRequestHandler<GetAllCustomersQuery, IEnumerable<CustomerDTO>>
    {
        public GetAllCustomersQueryHandler(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper) {}
        public async Task<IEnumerable<CustomerDTO>> Handle(GetAllCustomersQuery request, CancellationToken cancellationToken)
        {
            var customers = await _unitOfWork.Repository<Customer>().GetAllAsync();
            var customersDto = _mapper.Map<IEnumerable<CustomerDTO>>(customers);
            return customersDto;
        }
    }
}
using Domain.Enums;
using Domain.ValueObject;

namespace Application.Customers.Queries
{
    public class CustomerDTO
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string IdentityCard { get; set; }
        public Address Address { get; set; }
        public Gender Gender { get; set; }
    }
}
using Application.Places.Commands;
using Application.Places.Queries;
using Application.Customers.Queries;
using Application.TourAggregate.Commands.Create;
using Application.TourAggregate.Queries.DTOs;
using Application.TourAggregate.Queries.ViewModels;
using Application.TourTypes.Queries;
using AutoMapper;
using Domain.Entities;

namespace Application.Common.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile () {
            // Map TourType
            CreateMap<TourType, TourTypeDTO>();
            //Map Place
            CreateMap<Place, PlaceDTO>();
            CreateMap<CreatePlaceCommand, Place>();
            //Mapping TourAg Query
            CreateMap<TourProgram, TourProgramDTO>();
            CreateMap<TourProgram, TourProgramDetailViewModel>();
            CreateMap<Tour,TourDTO>();
            CreateMap<TourPlace, TourPlaceDTO>()
                .ForMember(dest => dest.PlaceName, act => act.MapFrom(src => src.Place.Name))
                .ReverseMap();
            CreateMap<Tour, TourListViewModel>();
            //Mapping TourAg Command
            CreateMap<CreateTourProgramCommand, TourProgram>();
            CreateMap<CreateTourCommand, Tour>();
            CreateMap<CreateTourPlaceCommand, TourPlace>();
            //Mapping Customer
            CreateMap<Customer, CustomerDTO>();
        }
    }
}
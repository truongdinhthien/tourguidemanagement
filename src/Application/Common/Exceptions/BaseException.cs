using System;

namespace Application.Common.Exceptions
{
    public class BaseException : Exception
    {
        public int Code { get; private set; }
        public string Description { get; private set; }

        public BaseException (string message, string description, int code) : base(message)
        {
            Code = code;
            Description = description;
        }
    }
}
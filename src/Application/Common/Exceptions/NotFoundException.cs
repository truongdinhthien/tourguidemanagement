namespace Application.Common.Exceptions
{
    public class NotFoundException : BaseException
    {
        public NotFoundException(string message, string description) : base(message, description, 404)
        {
        }
    }

}
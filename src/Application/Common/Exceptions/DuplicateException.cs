namespace Application.Common.Exceptions
{
    public class DuplicateException : BaseException
    {
        public DuplicateException(string message, string description) : base(message, description, 400)
        {
        }
    }

}
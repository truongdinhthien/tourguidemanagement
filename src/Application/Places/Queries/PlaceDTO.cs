namespace Application.Places.Queries
{
    public class PlaceDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
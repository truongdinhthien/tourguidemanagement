using Application.Common.Base;
using Application.Common.Interfaces;
using AutoMapper;
using Domain.Entities;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Places.Queries
{
    public class GetPlacesQuery : IRequest<IEnumerable<PlaceDTO>>
    {
        
    }

    public class GetPlacesQueryHandler : BaseHandler, IRequestHandler<GetPlacesQuery, IEnumerable<PlaceDTO>>
    {
        public GetPlacesQueryHandler(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper) {}
        public async Task<IEnumerable<PlaceDTO>> Handle(GetPlacesQuery request, CancellationToken cancellationToken)
        {
            var places = await _unitOfWork.Repository<Place>().GetAllAsync();
            var placesDto = _mapper.Map<IEnumerable<PlaceDTO>>(places);
            return placesDto;
        }
    }
}
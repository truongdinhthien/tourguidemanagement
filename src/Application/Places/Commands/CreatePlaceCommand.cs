using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Application.Common.Base;
using Application.Common.Interfaces;
using AutoMapper;
using Domain.Entities;
using MediatR;

namespace Application.Places.Commands
{
    public class CreatePlaceCommand : IRequest<int>
    {
        [Required]
        public string Name {get;set;}
    }

    public class CreatePlaceCommandHandler : BaseHandler, IRequestHandler<CreatePlaceCommand, int>
    {
        public CreatePlaceCommandHandler(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper) {}
        public async Task<int> Handle(CreatePlaceCommand request, CancellationToken cancellationToken)
        {
            var place = _mapper.Map<CreatePlaceCommand, Place>(request);
            await _unitOfWork.Repository<Place>().AddAsync(place);
            await _unitOfWork.SaveChangesAsync(cancellationToken);
            return place.Id;
        }
    }
}
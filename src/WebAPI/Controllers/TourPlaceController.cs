using System.Threading.Tasks;
using Application.TourAggregate.Commands.Create;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    public class TourPlaceController : ApiController
    {
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateTourPlaceCommand cmd)
        {
            return Ok(await Mediator.Send(cmd));
        }
    }
}
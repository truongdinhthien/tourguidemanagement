using System.Threading.Tasks;
using Application.TourAggregate.Commands.Create;
using Application.TourAggregate.Queries;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    public class TourController : ApiController
    {
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await Mediator.Send(new GetTourListQuery()));
        }
        [HttpGet("{id}/customers.json")]
        public async Task<IActionResult> GetCustomerFromTour(int? id)
        {
            return Ok(await Mediator.Send(new GetCustomerFromTourQuery() {TourId = id}));
        }
        [HttpPost]
        public async Task<IActionResult> Create(CreateTourCommand cmd)
        {
            return Ok(await Mediator.Send(cmd));
        }
    }
}
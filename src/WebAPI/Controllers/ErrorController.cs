using System;
using System.Net;
using Application.Common.Exceptions;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Common;

namespace WebAPI.Controllers
{
    [ApiController]
    public class ErrorController : ControllerBase
    {
        [Route("/error-local-development")]
        public ErrorReponse ErrorLocalDevelopment(
        [FromServices] IWebHostEnvironment webHostEnvironment)
        {
            if (webHostEnvironment.EnvironmentName != "Development")
            {
                throw new InvalidOperationException(
                    "This shouldn't be invoked in non-development environments.");
            }
            var context = HttpContext.Features.Get<IExceptionHandlerFeature>();
            var response = HttpContext.Response;
            var baseException = context.Error as BaseException;
            var statusCode = (int)HttpStatusCode.InternalServerError;
            var message = "Server Error";
            var description = context.Error.Message;

            if (null != baseException)
            {
                message = baseException.Message;
                description = baseException.Description;
                statusCode = baseException.Code;
            }

            response.ContentType = "application/json";
            response.StatusCode = statusCode;

            return new ErrorReponse() {
                Message = message,
                Description = description,
            };
        }

        [Route("/error")]
        public IActionResult Error() => Problem();
    }
}
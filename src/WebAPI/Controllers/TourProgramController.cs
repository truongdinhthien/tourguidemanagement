using System.Threading.Tasks;
using Application.TourAggregate.Commands.Create;
using Application.TourAggregate.Queries;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    public class TourProgramController : ApiController
    {
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await Mediator.Send(new GetTourProgramQuery()));
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetById([FromRoute] int id)
        {
            return Ok(await Mediator.Send(new GetTourProgramDetailByIdQuery() {Id = id}));
        }
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateTourProgramCommand cmd)
        {
            return Ok(await Mediator.Send(cmd));
        }
    }
}
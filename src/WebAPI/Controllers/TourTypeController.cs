﻿using System.Threading.Tasks;
using Application.TourTypes.Commands;
using Application.TourTypes.Queries;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    public class TourTypeController : ApiController
    {
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await Mediator.Send(new GetAllTourTypesQuery()));
        }
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateTourTypeCommand cmd)
        {
            return Ok(await Mediator.Send(cmd));


        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteTourTypeCommand { Id = id }));

        }
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, UpdateTourTypeCommand cmd)
        {
            cmd.Id = id;
            return Ok(await Mediator.Send(cmd));
        }
    }
}

using System.Threading.Tasks;
using Application.Places.Commands;
using Application.Places.Queries;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    public class PlaceController : ApiController
    {
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await Mediator.Send(new GetPlacesQuery()));
        }
        [HttpPost]
        public async Task<IActionResult> Create(CreatePlaceCommand cmd)
        {
            return Ok(await Mediator.Send(cmd));
        }
    }
}
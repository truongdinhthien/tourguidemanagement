namespace WebAPI.Common
{
    public class ErrorReponse
    {
        public bool Success { get; set; } = false;
        public string Message { get; set; }
        public string Description { get; set; }
    }
}
using System;
using System.Collections.Generic;
using Domain.Common;

namespace Domain.ValueObject
{
    public class Address : BaseValueObject
    {
        public String Street { get; private set; }
        public String City { get; private set; }
        public String District { get; private set; }
        public String Country { get; private set; }
        public String Ward { get; private set; }
        public Address() { }

        public Address(string street, string ward, string district, string city, string country)
        {
            Street = street;
            City = city;
            District = district;
            Country = country;
            Ward = ward;
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            // Using a yield return statement to return each element one at a time
            yield return Street;
            yield return City;
            yield return Ward;
            yield return Country;
            yield return District;
        }
    }
}
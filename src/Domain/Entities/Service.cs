using System.Collections.Generic;
using Domain.Common;

namespace Domain.Entities
{
    public class Service : BaseEntity
    {
        public decimal Price { get; set; }
        public string Name { get; set; }
        public ICollection<TourService> ServiceTours { get; set; }
    }
}
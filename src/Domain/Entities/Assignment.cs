using System;

namespace Domain.Entities
{
    public class Assignment
    {
        public Staff Staff { get; set; }
        public int StaffId { get; set; }
        public Tour Tour { get; set; }
        public int TourId { get; set; }
        public DateTime DateAssign {get;set;} = DateTime.UtcNow;
    }
}
using System.Collections.Generic;
using Domain.Common;

namespace Domain.Entities
{
    public class TourType : BaseEntity
    {
        public string Name { get; set; }
        public ICollection<TourProgram> TourPrograms { get; set; }
    }
}
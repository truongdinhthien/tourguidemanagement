using System;
using System.Collections.Generic;
using Domain.Common;
namespace Domain.Entities
{
    public class Tour : BaseEntity
    {
        public string Code { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public DateTime DateStart { get; set; }
        public int TourProgramId { get; set; }
        public TourProgram TourProgram { get; set; }
        public ICollection<Contract> Contracts { get; set; }
        public ICollection<Assignment> Assignments { get; set; }
        public decimal FinalPrice
        {
            get
            {
                if(TourProgram == null) return Price;
                return TourProgram.PriceService + TourProgram.PriceHotel + Price;
            }
            set {; }
        }
    }
}
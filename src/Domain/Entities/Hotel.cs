using System.Collections.Generic;
using Domain.Common;
using Domain.ValueObject;

namespace Domain.Entities
{
    public class Hotel : BaseEntity
    {
        public string Name { get; set; }
        public Address Address { get; set; }
        public ICollection<TourProgram> TourPrograms { get; set; }
    }
}
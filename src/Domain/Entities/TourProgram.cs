using System;
using System.Collections.Generic;
using Domain.Common;

namespace Domain.Entities
{
    public class TourProgram : BaseEntity
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int Duration { get; set; }
        public int TourTypeId { get; set; }
        public TourType TourType { get; set; }
        public int HotelId { get; set; }
        public Hotel Hotel { get; set; }
        public decimal PriceHotel {get;set;}
        public ICollection<TourPlace> TourPlaces { get; set; }
        public ICollection<TourService> TourServices {get;set;}
        public ICollection<Tour> Tours { get; set; }
        public decimal PriceService
        {
            get
            {
                decimal priceSumService = 0;
                if(TourServices == null) return priceSumService;
                
                foreach (var item in TourServices)
                {
                    if(item.Service == null) return priceSumService;
                    priceSumService += item.Service.Price;
                }
                return priceSumService;
            }
            set {; }
        }
    }
}
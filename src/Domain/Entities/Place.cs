using System.Collections.Generic;
using Domain.Common;

namespace Domain.Entities
{
    public class Place : BaseEntity
    {
        public string Name {get;set;}
        public ICollection<TourPlace> TourPlaces { get; set; }
    }
}
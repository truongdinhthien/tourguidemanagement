namespace Domain.Entities
{
    public class TourPlace
    {
        public int TourProgramId { get; set; }
        public TourProgram TourProgram { get; set; }
        public int PlaceId { get; set; }
        public Place Place { get; set; }
        public int OrdinalNumber { get; set; }
    }
}
using System.Collections.Generic;
using Domain.Common;
using Domain.Enums;
using Domain.ValueObject;

namespace Domain.Entities
{
    public class Customer : BaseEntity
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string IdentityCard { get; set; }
        public Address Address { get; set; }
        public Gender Gender { get; set; }
        public ICollection<Contract> Contracts {get;set;}
    }
}
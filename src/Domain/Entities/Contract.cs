using System;
using Domain.Common;

namespace Domain.Entities
{
    public class Contract : BaseEntity
    {
        public int TourId {get;set;}
        public int CustomerId {get;set;}
        public Tour Tour {get;set;}
        public Customer Customer {get;set;}
        public DateTime CreatedDate {get;set;} = DateTime.UtcNow;
        public int Amount {get;set;}
        public decimal Price {
            get {
                return Tour.Price * Amount;
            }
        }
    }
}
namespace Domain.Entities
{
    public class TourService
    {
        public int ServiceId { get; set; }
        public Service Service { get; set; }
        public int TourProgramId { get; set; }
        public TourProgram TourProgram { get; set; }
    }
}
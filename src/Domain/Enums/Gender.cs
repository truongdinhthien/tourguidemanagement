using Domain.Common;

namespace Domain.Enums
{
    public enum Gender {
        Male, 
        Female,
        Other,
    }
}
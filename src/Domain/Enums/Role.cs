using Domain.Common;

namespace Domain.Enums
{
    public enum Role {
        BusDriver,
        Leader,
        TourGuide,
    }
}
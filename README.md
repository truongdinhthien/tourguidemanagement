## Structure Folder

### Domain 
Chứa toàn bộ các Entity, Enum, Exception, Type và Login đặc biệc ở Domain

### Application 
Chứa đa số các nghiệp vụ của hệ ( Application Service ). Phụ thuộc vào tầng Domain và đinh nghĩa các Interface được kế thừa từ tàng khác ( Đa số là tàng Infras ). Ví dụ nếu ứng dụng cần upload file thì sẽ khởi tạo một IFileHandle, sau đó FileHandle kế thừa tại tầng Infras

### Infrastructure
Tầng chứa các thứ liên quan về xử lý logic system như là database, lưu trữ file, caching, tracking, ... Các class của tàng này đều được định nghĩa và kế thừa dựa vào Interface của tầng Application

### WebAPI (Presentation)
Đây là tầng xem như là hiển thị, có thể là webapi là một mvc web là một winform hoặc bất cứ thứ gì. Tầng này phụ thuộc hoàn toàn vào tầng Application và tầng Infras, tuy nhiên chỉ phụ thuộc là tầng Infras lúc compile time để tiêm các denpendency chứ không phụ thuốc bất kì logic nào lúc run time


## Design Pattern

Lý do có các pattern này cũng là chỉ giúp tuân thủ nguyên tắc DONT REPEAT YOURSELF

### Repository and UnitOfWork Pattern 
Mục đich là tạo ra một lớp trừu tượng giữa tầng Infras và tầng Application. Thằng này xài ban đầu hơi cực nhưng lâu dài thì sẽ sướng, giúp tập trung hơn vào tầng Application.
xem thêm tại : https://docs.microsoft.com/en-us/dotnet/architecture/microservices/microservice-ddd-cqrs-patterns/infrastructure-persistence-layer-design

![](docs/repository-pattern.png)

### Spefication Pattern
Pattern này giúp đóng gói lại vài thông tin nghiệp vụ riêng biệt vào trong một nơi và sử dụng lại chùng ở nơi khác. 

### CQRS Pattern
Thằng này chưa Implement xong. Cơ bản thì sẽ tách việc đọc ghi thành 2 luồng logic và lưu DB ( thường đọc sẽ là đọc từ cache, và ghi là ghi vào DB). Hiện tại sử dụng nuget MediatR để làm

### DDD
DDD thì là một phương pháp luận về việc code, nó lớn hơn một Design Pattern. Nếu Implement hoàn chỉnh được DDD thì sẽ thấy CQRS là chân lý. CQRS + DDD = <3

### Observer Pattern
Có thời gian thì làm. Pattern này giúp tổ chức việc tracking người dùng hệ thống tốt nhất

## Project

### Run

1. Tải phiên bản mới nhất của [.NET Core SDK](https://dotnet.microsoft.com/download)
2. Trỏ tới `src/WebUI` tạo file `appsettings.Development.json` có cấu trúc y chang `appsettings.json`, từ đó thay đổi các biến môi trường tại local trong file vừa (ví dụ ConnectionString )
3. Gõ câu lệnh `dotnet watch run` để chạy backend (ASP.NET Core Web API)

### Database Migrations
Tạo mới một Migrations tại WebUI folder

Hạn chế xóa folder Migrations, hãy đối xử với nó như là các commit của github
```
dotnet ef migrations add NewMigration -p ../Infrastructure  -o Persistence/Migrations
```